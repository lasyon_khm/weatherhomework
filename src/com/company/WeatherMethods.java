package com.company;

import java.util.*;

public class WeatherMethods implements WeatherInterface {
    List<Weather> weathersList;

    public WeatherMethods(List<Weather> weatherList) {
        this.weathersList = weatherList;
    }


    @Override
    public Double countAverageTemp() {
        double averageTemp;
        double sum = 0.0;

        for (Weather weatherList : weathersList) {
            sum += weatherList.getTemperature();
        }
        averageTemp = sum / weathersList.size();

        return averageTemp;
    }

    @Override
    public Double countAverageHumidity() {
        double averageHumidity;
        double sum = 0.0;

        for (Weather weatherList : weathersList) {
            sum += weatherList.getHumidity();
        }
        averageHumidity = sum / weathersList.size();

        return averageHumidity;
    }

    @Override
    public Double countAverageWindSpeed() {
        double averageSpeed;
        double sum = 0.0;

        for (Weather weatherList : weathersList) {
            sum += weatherList.getWindSpeed();
        }
        averageSpeed = sum / weathersList.size();

        return averageSpeed;
    }

    @Override
    public Weather maxTempDay() {
        double maxTemp;
        List<Double> tempList = new ArrayList<>();
        for (Weather weatherList : weathersList) {
            tempList.add(weatherList.getTemperature());
        }
        maxTemp = Collections.max(tempList);
        for (Weather weatherList : weathersList) {
            if (weatherList.getTemperature() == maxTemp) {
                return weatherList;
            }
        }
        return null;
    }

    @Override
    public Weather minHumidityDay() {
        double minHumidity;
        List<Double> tempList = new ArrayList<>();
        for (Weather weatherList : weathersList) {
            tempList.add(weatherList.getHumidity());
        }
        minHumidity = Collections.min(tempList);
        for (Weather weatherList : weathersList) {
            if (weatherList.getHumidity() == minHumidity) {
                return weatherList;
            }
        }
        return null;
    }

    @Override
    public Weather maxWindSpeedDay() {
        double maxSpeed;
        List<Double> tempList = new ArrayList<>();
        for (Weather weatherList : weathersList) {
            tempList.add(weatherList.getWindSpeed());
        }
        maxSpeed = Collections.max(tempList);
        for (Weather weatherList : weathersList) {
            if (weatherList.getWindSpeed() == maxSpeed) {
                return weatherList;
            }
        }
        return null;
    }

    @Override
    public String mostFrequentWindDirection() {
        TreeMap<String, Integer> directions = new TreeMap<>();
        int west = 0;
        int east = 0;
        int north = 0;
        int south = 0;

        for(Weather weatherList : weathersList){
            if(weatherList.getWindDirection() <= 45 || weatherList.getWindDirection() > 315) north++;
            if(weatherList.getWindDirection() > 45 && weatherList.getWindDirection() <= 135) east++;
            if(weatherList.getWindDirection() > 135 && weatherList.getWindDirection() < 225) south++;
            if (weatherList.getWindDirection() > 225 && weatherList.getWindDirection() < 315) west++;
        }
        directions.put("W", west);
        directions.put("N", north);
        directions.put("E", east);
        directions.put("S", south);


        return directions.lastKey();
    }
}
