package com.company;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Main {
    private final static String PATH_TO_FILE = "src/com/company/weather_table.csv";

    public static void main(String[] args) throws Exception {
        FileManager fileManager = new FileManager(new File(Paths.get(PATH_TO_FILE).toUri()));
        List<Weather> weatherList = fileManager.readingFromFile();
        WeatherMethods weatherMethods = new WeatherMethods(weatherList);

        File file = new File("answer.txt");

        if(!file.exists()){
            file.createNewFile();
        }


        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        bufferedWriter.write("Средняя температура : " + weatherMethods.countAverageTemp() + "°C" +"\n");
        bufferedWriter.write("Средняя влажность : " + weatherMethods.countAverageHumidity() + "%" + "\n");
        bufferedWriter.write("Средняя скорость ветра : " + weatherMethods.countAverageWindSpeed() + " km/h" + "\n");

        bufferedWriter.newLine();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM, HH:mm");

        Weather maxTempWeather = weatherMethods.maxTempDay();
        Date maxTempDay = maxTempWeather.getDate();
        bufferedWriter.write("Максимальная температура  " + maxTempWeather.getTemperature() + "°C была зафиксирована " + simpleDateFormat.format(maxTempDay) + "\n");

        Weather minHumidityWeather = weatherMethods.minHumidityDay();
        Date minHumDay = minHumidityWeather.getDate();
        bufferedWriter.write("Минимальная влажность  " + minHumidityWeather.getHumidity() + "% была зафиксирована " + simpleDateFormat.format(minHumDay) + "\n");

        Weather maxSpeedWeather = weatherMethods.maxWindSpeedDay();
        Date maxSpeedDay = maxSpeedWeather.getDate();
        bufferedWriter.write("Максимальная скорость ветра  " + maxSpeedWeather.getWindSpeed() + " km/h была зафиксирована " + simpleDateFormat.format(maxSpeedDay) + "\n");

        bufferedWriter.newLine();

        bufferedWriter.write("Самое частое направление ветра: " + weatherMethods.mostFrequentWindDirection());


        bufferedWriter.close();

    }
}
