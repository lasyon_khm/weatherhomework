package com.company;

import java.util.Date;

public class Weather {
    private Date date;
    private Double temperature;
    private Double humidity;
    private Double windSpeed;
    private Double windDirection;

    public Weather(Date date, Double temperature, Double humidity, Double windSpeed, Double windDirection) {
        this.date = date;
        this.temperature = temperature;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
    }

    public Date getDate() {
        return date;
    }

    public double getTemperature() {
        return temperature;
    }

    public double getHumidity() {
        return humidity;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public double getWindDirection() {
        return windDirection;
    }


    @Override
    public String toString() {
        return "Weather{" +
                "date=" + date +
                ", temperature=" + temperature +
                ", humidity=" + humidity +
                ", windSpeed=" + windSpeed +
                ", windDirection=" + windDirection +
                '}';
    }
}
