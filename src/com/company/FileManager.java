package com.company;

import java.io.File;
import java.util.*;
import java.text.SimpleDateFormat;

public class FileManager {
   private File answerFile;

    public FileManager(File answerFile) {
        this.answerFile = answerFile;
    }

    public List<Weather> readingFromFile() throws Exception {
        List<Weather> weathers = new ArrayList<>();

        Scanner scanner = new Scanner(answerFile);
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            String[] parts = line.split(",");
            if (parts[0].charAt(0) >= '0' && parts[0].charAt(0) <= '9') {
                Weather weather = deserializeWeather(parts);
                weathers.add(weather);
            }
        }

        return weathers;
    }

    private static Weather deserializeWeather(String[] parts) throws Exception {
        Weather weather;
        if (parts.length < 5) throw new Exception("not less than 5");
        else {
            String date = parts[0];
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmm");
            Date correctDate = simpleDateFormat.parse(date);
            Double temperature = Double.parseDouble(parts[1]);
            Double humidity = Double.parseDouble(parts[2]);
            Double windSpeed = Double.parseDouble(parts[3]);
            Double windDirection = Double.parseDouble(parts[4]);

            weather = new Weather(correctDate, temperature, humidity, windSpeed, windDirection);
        }
        return weather;
    }
}
