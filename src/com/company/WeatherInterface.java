package com.company;

public interface WeatherInterface {
    Double countAverageTemp();

    Double countAverageHumidity();

    Double countAverageWindSpeed();

    Weather maxTempDay();

    Weather minHumidityDay();

    Weather maxWindSpeedDay();

    String mostFrequentWindDirection();
}
